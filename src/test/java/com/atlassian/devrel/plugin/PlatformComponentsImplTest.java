package com.atlassian.devrel.plugin;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;
import org.osgi.service.packageadmin.ExportedPackage;
import org.osgi.service.packageadmin.PackageAdmin;

import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlatformComponentsImplTest {
    @Mock(answer = RETURNS_DEEP_STUBS)
    PluginAccessor accessor;
    @Mock(answer = RETURNS_DEEP_STUBS)
    BundleContext bundleContext;

    @Test
    public void versionsWorkMaybe() {
        ExportedPackage xp = mock(ExportedPackage.class);
        when(xp.getVersion()).thenReturn(new Version("0.0.1"));
        mockSalCheck(xp);
        mockPluginVersion("com.atlassian.activeobjects.activeobjects-plugin", "1.2.3");

        Map<String,String> versions = new PlatformComponentsImpl(accessor, bundleContext).getPlatformComponents();
        assertThat(versions.get("Shared Application Layer (SAL)"), equalTo("0.0.1"));
        assertThat(versions.get("Active Objects"), equalTo("1.2.3"));
    }

    @Test
    public void versionsNotIncludedIfNotEnabled() {
        mockSalCheck(null);
        mockPluginVersion("com.atlassian.activeobjects.activeobjects-plugin", null);

        Map<String,String> versions = new PlatformComponentsImpl(accessor, bundleContext).getPlatformComponents();
        assertThat(versions.containsKey("Shared Application Layer (SAL)"), is(false));
        assertThat(versions.containsKey("Active Objects"), is(false));
    }

    private void mockSalCheck(final ExportedPackage xp) {
        PackageAdmin p = mock(PackageAdmin.class);
        when(bundleContext.getService(any(ServiceReference.class))).thenReturn(p);
        when(p.getExportedPackage("com.atlassian.sal.api")).thenReturn(xp);
    }

    private void mockPluginVersion(final String moduleKey, final String version) {
        Plugin p = mock(Plugin.class, RETURNS_DEEP_STUBS);
        when(p.getPluginInformation().getVersion()).thenReturn(version);
        Plugin yield = version != null ? p : null;
        when(accessor.getPlugin(moduleKey)).thenReturn(yield);
        when(accessor.getEnabledPlugin(moduleKey)).thenReturn(yield);
    }
}
