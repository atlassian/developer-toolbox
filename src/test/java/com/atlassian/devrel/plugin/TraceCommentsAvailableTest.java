package com.atlassian.devrel.plugin;

import com.atlassian.devrel.checks.PluginChecks;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInformation;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static com.atlassian.devrel.checks.PluginChecks.ATR_VELOCITY_PLUGIN_KEY;
import static com.atlassian.devrel.checks.PluginChecks.SOY_PLUGIN_KEY;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TraceCommentsAvailableTest {
    private static final String UNSUPPORTED_ATR_VELOCITY_VERSION = "6.1.3";
    private static final String UNSUPPORTED_SOY_VERSION = "7.0.4";
    private static final String SUPPORTED_ATR_VELOCITY_VERSION = "6.2.0";
    private static final String SUPPORTED_SOY_VERSION = "7.1.0";

    @Rule
    public
    MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    PluginAccessor pluginAccessor;

    @InjectMocks
    PluginChecks pluginChecks;

    @Test
    public void testUnsupportedVersions_shouldReturnFalse() {
        givenPluginOfVersion(ATR_VELOCITY_PLUGIN_KEY, UNSUPPORTED_ATR_VELOCITY_VERSION);
        givenPluginOfVersion(SOY_PLUGIN_KEY, UNSUPPORTED_SOY_VERSION);

        assertFalse(pluginChecks.isTraceCommentsAvailable());
    }

    @Test
    public void testOnlySoyIsSupported_shouldReturnTrue() {
        givenPluginOfVersion(ATR_VELOCITY_PLUGIN_KEY, UNSUPPORTED_ATR_VELOCITY_VERSION);
        givenPluginOfVersion(SOY_PLUGIN_KEY, SUPPORTED_SOY_VERSION);

        assertTrue(pluginChecks.isTraceCommentsAvailable());
    }

    @Test
    public void testOnlyAtrVelocityIsSupported_shouldReturnTrue() {
        givenPluginOfVersion(ATR_VELOCITY_PLUGIN_KEY, SUPPORTED_ATR_VELOCITY_VERSION);
        givenPluginOfVersion(SOY_PLUGIN_KEY, UNSUPPORTED_SOY_VERSION);

        assertTrue(pluginChecks.isTraceCommentsAvailable());
    }

    @Test
    public void testDoubleDigitMinorVersions_shouldNotFoolTheComparison() {
        givenPluginOfVersion(ATR_VELOCITY_PLUGIN_KEY, "6.10.0-SNAPSHOT");
        givenPluginOfVersion(SOY_PLUGIN_KEY, UNSUPPORTED_SOY_VERSION);

        assertTrue(pluginChecks.isTraceCommentsAvailable());
    }

    private void givenPluginOfVersion(String pluginKey, String version) {
        Plugin mockSoyPlugin = mock(Plugin.class);
        PluginInformation mockSoyPluginInformation = mock(PluginInformation.class);
        when(pluginAccessor.getEnabledPlugin(pluginKey)).thenReturn(mockSoyPlugin);
        when(mockSoyPlugin.getPluginInformation()).thenReturn(mockSoyPluginInformation);
        when(mockSoyPluginInformation.getVersion()).thenReturn(version);
    }
}
