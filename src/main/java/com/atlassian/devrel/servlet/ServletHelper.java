package com.atlassian.devrel.servlet;

import com.atlassian.devrel.checks.PluginChecks;
import com.atlassian.devrel.plugin.PlatformComponents;
import com.atlassian.devrel.util.TextUtils;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import com.atlassian.webresource.api.assembler.WebResourceSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.devrel.servlet.ServletHelper.Application.BAMBOO;
import static com.atlassian.devrel.servlet.ServletHelper.Application.BITBUCKET;
import static com.atlassian.devrel.servlet.ServletHelper.Application.CONFLUENCE;
import static com.atlassian.devrel.servlet.ServletHelper.Application.CRUCIBLE;
import static com.atlassian.devrel.servlet.ServletHelper.Application.FISHEYE;
import static com.atlassian.devrel.servlet.ServletHelper.Application.JIRA;
import static com.atlassian.devrel.servlet.ServletHelper.Application.STASH;
import static com.atlassian.devrel.servlet.ServletHelper.Application.UNKNOWN;
import static com.atlassian.sal.api.UrlMode.RELATIVE;
import static java.lang.String.format;
import static java.util.Locale.ENGLISH;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

public class ServletHelper {

    static final String CONTENT_TYPE = "text/html;charset=utf-8";

    private final ApplicationProperties applicationProperties;
    private final DynamicWebInterfaceManager dynamicWebInterfaceManager;
    private final LoginUriProvider loginUriProvider;
    private final PlatformComponents platformComponents;
    private final PluginAccessor pluginAccessor;
    private final PluginChecks pluginChecks;
    private final UserManager userManager;
    private final TemplateRenderer templateRenderer;
    private final TextUtils textUtils;
    private final WebResourceAssemblerFactory webResourceAssemblerFactory;

    public ServletHelper(
            final ApplicationProperties applicationProperties,
            final DynamicWebInterfaceManager dynamicWebInterfaceManager,
            final LoginUriProvider loginUriProvider,
            final PlatformComponents platformComponents,
            final PluginAccessor pluginAccessor,
            final PluginChecks pluginChecks,
            final UserManager userManager,
            final TemplateRenderer templateRenderer,
            final TextUtils textUtils,
            final WebResourceAssemblerFactory webResourceAssemblerFactory) {
        this.applicationProperties = requireNonNull(applicationProperties, "applicationProperties");
        this.dynamicWebInterfaceManager = requireNonNull(dynamicWebInterfaceManager, "dynamicWebInterfaceManager");
        this.loginUriProvider = requireNonNull(loginUriProvider, "loginUriProvider");
        this.platformComponents = requireNonNull(platformComponents, "platformComponents");
        this.pluginAccessor = requireNonNull(pluginAccessor, "pluginAccessor");
        this.pluginChecks = requireNonNull(pluginChecks, "pluginChecks");
        this.userManager = requireNonNull(userManager, "userManager");
        this.templateRenderer = requireNonNull(templateRenderer, "templateRenderer");
        this.textUtils = requireNonNull(textUtils, "textUtils");
        this.webResourceAssemblerFactory = requireNonNull(webResourceAssemblerFactory, "webResourceAssemblerFactory");
    }

    Application getApplication() {
        String displayName = applicationProperties.getDisplayName();
        for (Application application : Application.values()) {
            if (application.name().equalsIgnoreCase(displayName)) {
                if (application == STASH) {
                    application = BITBUCKET;
                }
                return application;
            }
        }
        return UNKNOWN;
    }

    public String respond(
            @Nonnull final String baseRequestUrl,
            @Nullable String requestQueryString,
            @Nonnull PrintWriter printWriter,
            @Nonnull Application application,
            boolean i18nDebugEnabled,
            @Nonnull Map<String, String[]> requestParameters) throws IOException {
        final String templateFileName;
        final Map<String, Object> context = new HashMap<>();

        context.put("applicationProperties", applicationProperties);
        context.put("textUtils", textUtils);

        if (baseRequestUrl.contains("developer-toolbox")) {
            templateFileName = "/templates/homepage.vm";

            if (isNotSystemAdmin()) {
                return getLoginUrl(baseRequestUrl, requestQueryString);
            }

            boolean available = application == JIRA || application == CONFLUENCE;
            context.put("i18nAvailable", available);
            if (available) {
                context.put("i18nActive", i18nDebugEnabled);
            }

            context.put("restApiBrowserAvailable", pluginAccessor.getEnabledPlugin("com.atlassian.labs.rest-api-browser") != null);
            context.put("sdkVersion", System.getProperty("atlassian.sdk.version", "3.7 or earlier"));
            context.put("qrAvailable", pluginChecks.isQuickReloadAvailable());
            context.put("aoAvailable", pluginChecks.isActiveObjectsAvailable());
            context.put("pdeAvailable", pluginChecks.isPluginDataEditorAvailable());
            context.put("systemInfoLink", getSystemInfoLink(application));
            context.put("platformComponents", platformComponents.getPlatformComponents());
            context.put("javadocUrl", getJavadocLink(application));
            context.put("restApisUrl", getRestApisLink(application));
            context.put("appDevUrl", getAppDevDocLink(application));
            context.put("displayName", applicationProperties.getDisplayName());
            context.put("version", applicationProperties.getVersion());
            context.put("baseUrl", getBaseUrl());
            context.put("product", application.slug());

            final String applicationSlug = application == UNKNOWN
                    ? applicationProperties.getDisplayName().toLowerCase(ENGLISH)
                    : application.slug();
            context.put("downloadUrl", format("http://www.atlassian.com/software/%s/download?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox", applicationSlug));
        } else if (baseRequestUrl.contains("dev-toolbar")) {
            templateFileName = "/templates/toolbar.vm";

            context.put("navBarItems", asList(dynamicWebInterfaceManager.getDisplayableWebItems("dev-toolbar", new HashMap<>())));
            context.put("navBarMenuItems", asList(dynamicWebInterfaceManager.getDisplayableWebItems("dev-toolbar-menu", new HashMap<>())));
            context.put("app", applicationProperties);
            context.put("sdkVersion", System.getProperty("atlassian.sdk.version", "3.7 or earlier"));
            context.put("devToolboxVersion", pluginChecks.getDevtoolboxVersion());
            context.put("isTraceCommentsAvailable", pluginChecks.isTraceCommentsAvailable());
            context.put("quickreloadVersion", pluginChecks.getQuickReloadVersion());
        } else {
            final boolean includeSuperbatch = getBoolean(safeSingle(requestParameters.get("super")), false);
            final boolean allowAsync = getBoolean(safeSingle(requestParameters.get("async")), false);
            final List<String> contexts = safe(requestParameters.get("context"));
            final List<String> resources = safe(requestParameters.get("resource"));

            if (allowAsync) {
                resources.add("com.atlassian.plugins.atlassian-plugins-webresource-plugin:web-resource-manager");
            }

            // Open the response.
            printWriter.write(
                    "<!doctype html>" +
                            "<html>" +
                            "<head>" +
                            "    <title>An empty page</title>" +
                            "</head>" +
                            "<body>" +
                            "    <p>You can use this page to test UI features in isolation!</p>" +
                            "    <p>Try the following things:</p>" +
                            "    <dl>" +
                            "        <dt>Load the superbatch</dt>" +
                            "        <dd><a href='?super=true'>Set <var>?super=true</var> in the URL</a></dd>" +

                            "        <dt>Load <var>WRM.require</var></dt>" +
                            "        <dd><a href='?async=true'>Set <var>?async=true</var> in the URL</a></dd>" +

                            "        <dt>Load a specific web-resource</dt>" +
                            "        <dd>Set <var>resource=[webresource-key]</var> in the URL</var></dd>" +

                            "        <dt>Load a specific web-resource context</dt>" +
                            "        <dd>Set <var>context=[webresource-key]</var> in the URL</var></dd>" +

                            "    </dl>"
            );
            printWriter.flush();


            WebResourceAssembler webResourceAssembler = webResourceAssemblerFactory.create()
                    .autoIncludeFrontendRuntime(false)
                    .includeSuperbatchResources(includeSuperbatch)
                    .build();
            if (!includeSuperbatch) {
                webResourceAssembler.resources().excludeSuperbatch();
            }

            for (String ctx : contexts) {
                webResourceAssembler.resources().requireContext(ctx);
            }
            for (String res : resources) {
                webResourceAssembler.resources().requireWebResource(res);
            }

            // Drain anything that should be loaded.
            final WebResourceSet webResourceSet = webResourceAssembler.assembled().drainIncludedResources();
            final Writer webResourceWriter = new StringWriter();
            webResourceSet.writeHtmlTags(webResourceWriter, UrlMode.AUTO);
            final String webResourceHtml = webResourceWriter.toString();

            if (!webResourceHtml.isEmpty()) {
                printWriter.write(webResourceHtml);
                printWriter.write("<p>The requested resources have been loaded!</p>");
            }

            if (allowAsync) {
                printWriter.write(
                        "    <p>There should be a <code>WRM.require</code> function on the page now.</p>\n" +
                                "    <p>Try running something like:</p>\n" +
                                "    <pre><code>WRM.require(['com.atlassian.auiplugin:ajs'], function() {\n" +
                                "        console.log('loaded', {AJS});\n" +
                                "    })</code></pre>\n"
                );
            }
            printWriter.flush();

            printWriter.write("</body></html>");
            printWriter.close();
            return null;
        }

        templateRenderer.render(templateFileName, context, printWriter);
        return null;
    };

    private boolean isNotSystemAdmin() {
        UserKey remoteUserKey = userManager.getRemoteUserKey();
        return isNull(remoteUserKey) || !userManager.isSystemAdmin(remoteUserKey);
    }

    @Nonnull
    private String getLoginUrl(@Nonnull final String baseRequestUrl, @Nullable String requestQueryString) {
        String requestUrl = baseRequestUrl;
        if (requestQueryString != null) {
            requestUrl += '?' + requestQueryString;
        }

        return loginUriProvider.getLoginUri(URI.create(requestUrl)).toASCIIString();
    }



    private String getAppDevDocLink(Application application) {
        switch (application) {
            case CONFLUENCE:
                return "https://developer.atlassian.com/server/confluence/confluence-plugin-guide/";
            case JIRA:
                return "https://developer.atlassian.com/server/jira/platform/getting-started/";
            case STASH:
            case BITBUCKET:
                return "https://developer.atlassian.com/server/bitbucket/how-tos/beginner-guide-to-bitbucket-server-plugin-development/";
            case BAMBOO:
                return "https://developer.atlassian.com/server/bamboo/";
            case CRUCIBLE:
            case FISHEYE:
                return "https://developer.atlassian.com/server/fisheye-crucible/";
            case CROWD:
                return "https://developer.atlassian.com/server/crowd/";
            default:
                return "https://developer.atlassian.com/server/";
        }
    }

    private String getRestApisLink(Application application) {
        switch (application) {
            case CONFLUENCE:
                return "https://docs.atlassian.com/ConfluenceServer/rest/latest/";
            case JIRA:
                return "https://docs.atlassian.com/software/jira/docs/api/REST/latest/";
            case STASH:
            case BITBUCKET:
                return "https://developer.atlassian.com/server/bitbucket/reference/rest-api/";
            case BAMBOO:
                return "https://developer.atlassian.com/server/bamboo/rest/";
            case CRUCIBLE:
            case FISHEYE:
                return "https://developer.atlassian.com/server/fisheye-crucible/rest-api-guide/";
            case CROWD:
                return "https://docs.atlassian.com/atlassian-crowd/latest/REST/";
            default:
                return null;
        }
    }

    private String getJavadocLink(Application application) {
        switch (application) {
            case CONFLUENCE:
                return "https://developer.atlassian.com/server/confluence/java-api-reference/";
            case JIRA:
                return "https://developer.atlassian.com/server/jira/platform/java-apis/";
            case STASH:
            case BITBUCKET:
                return "https://developer.atlassian.com/server/bitbucket/reference/java-api/";
            case BAMBOO:
                return "https://developer.atlassian.com/server/bamboo/java-api-reference/";
            case CRUCIBLE:
            case FISHEYE:
                return "https://developer.atlassian.com/server/fisheye-crucible/java-api-reference/";
            case CROWD:
                return "http://docs.atlassian.com/atlassian-crowd/latest/";
            default:
                return null;
        }
    }

    private <T> List<T> asList(Iterable<T> iterable) {
        return stream(iterable.spliterator(), false).collect(toList());
    }

    private String getSystemInfoLink(Application application) {
        if (application == JIRA) {
            return getBaseUrl() + "/secure/admin/ViewSystemInfo.jspa";
        } else if (application == CONFLUENCE || application == BAMBOO) {
            return getBaseUrl() + "/admin/systeminfo.action";
        } else if (application == FISHEYE || application == CRUCIBLE) {
            return getBaseUrl() + "/admin/sysinfo.do";
        } else {
            // can't get right now
            return null;
        }
    }

    private String getBaseUrl() {
        return applicationProperties.getBaseUrl(RELATIVE);
    }

    private boolean getBoolean(final String val, final boolean defaultVal) {
        if (val == null) {
            return defaultVal;
        }
        return Boolean.parseBoolean(val);
    }

    @Nullable
    private String safeSingle(@Nullable final String[] input) {
        return isNull(input) ? null : input[0];
    }

    private List<String> safe(final String[] input) {
        List<String> results = new ArrayList<>();
        if (input != null) {
            for (String val : input) {
                if (val != null && !val.isEmpty()) {
                    results.add(val);
                }
            }
        }
        return results;
    }

    enum Application {
        JIRA, CONFLUENCE, CROWD, FISHEYE, CRUCIBLE, BAMBOO, STASH, BITBUCKET, UNKNOWN;

        String slug() {
            return this.name().toLowerCase(ENGLISH);
        }
    }
}
