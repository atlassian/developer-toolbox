package com.atlassian.devrel.servlet;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.devrel.servlet.ServletHelper.Application;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

import static com.atlassian.devrel.servlet.ServletHelper.Application.CONFLUENCE;
import static com.atlassian.devrel.servlet.ServletHelper.Application.JIRA;
import static com.atlassian.devrel.servlet.ServletHelper.CONTENT_TYPE;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

/**
 * Serves the developer toolbox home page.
 */
@UnrestrictedAccess
public class JakartaServlet extends HttpServlet  {

    private final ServletHelper servletHelper;

    public JakartaServlet(ServletHelper servletHelper) {
        this.servletHelper = requireNonNull(servletHelper, "servletHelper");
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final Application application = servletHelper.getApplication();
        final boolean i18nDebugEnabled = getI18nState(request, application);
        final String loginUrl = servletHelper.respond(request.getRequestURI(), request.getQueryString(), response.getWriter(), application, i18nDebugEnabled, request.getParameterMap());
        if (isNull(loginUrl)) {
            response.setContentType(CONTENT_TYPE);
        } else {
            response.sendRedirect(loginUrl);
        }
    }

    private boolean getI18nState(HttpServletRequest request, Application application) {
        HttpSession session = request.getSession(false);
        if (application == JIRA) {
            return session.getAttribute("com.atlassian.jira.util.i18n.I18nTranslationModeSwitch") != null;
        } else if (application == CONFLUENCE) {
            Object o = session.getAttribute("confluence.i18n.mode");
            if (o != null) {
                return o.getClass().getSimpleName().equals("LightningTranslationMode");
            }
        }

        return false;
    }
}
