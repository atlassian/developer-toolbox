package com.atlassian.devrel.spring;

import com.atlassian.devrel.checks.PluginChecks;
import com.atlassian.devrel.plugin.PlatformComponents;
import com.atlassian.devrel.plugin.PlatformComponentsImpl;
import com.atlassian.devrel.servlet.ServletHelper;
import com.atlassian.devrel.util.TextUtils;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import org.osgi.framework.BundleContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringConfiguration {

    // OSGi Service Imports
    @Bean("application-properties")
    public ApplicationProperties ApplicationProperties() {
        return importOsgiService(ApplicationProperties.class);
    }

    @Bean("dynamicWebInterfaceManager")
    public DynamicWebInterfaceManager DynamicWebInterfaceManager() {
        return importOsgiService(DynamicWebInterfaceManager.class);
    }

    @Bean("loginUriProvider")
    public LoginUriProvider LoginUriProvider() {
        return importOsgiService(LoginUriProvider.class);
    }

    @Bean
    public PluginAccessor pluginAccessor() {
        return importOsgiService(PluginAccessor.class);
    }

    @Bean("template-renderer")
    public TemplateRenderer VelocityTemplateRenderer() {
        return importOsgiService(TemplateRenderer.class);
    }

    @Bean("userManager")
    public UserManager UserManager() {
        return importOsgiService(UserManager.class);
    }

    @Bean("factory")
    public WebResourceAssemblerFactory webResourceAssemblerFactory() {
        return importOsgiService(WebResourceAssemblerFactory.class);
    }

    // Beans
    @Bean("platformComponentsAccessor")
    public PlatformComponents platformComponents(BundleContext bundleContext, PluginAccessor pluginAccessor) {
        return new PlatformComponentsImpl(pluginAccessor, bundleContext);
    }

    @Bean("pluginChecks")
    public PluginChecks pluginChecks(PluginAccessor pluginAccessor) {
        return new PluginChecks(pluginAccessor);
    }

    @Bean
    public ServletHelper servletHelper(
            final ApplicationProperties applicationProperties,
            final DynamicWebInterfaceManager dynamicWebInterfaceManager,
            final LoginUriProvider loginUriProvider,
            final PlatformComponents platformComponents,
            final PluginAccessor pluginAccessor,
            final PluginChecks pluginChecks,
            final UserManager userManager,
            final TemplateRenderer templateRenderer,
            final TextUtils textUtils,
            final WebResourceAssemblerFactory webResourceAssemblerFactory) {
        return new ServletHelper(
                applicationProperties,
                dynamicWebInterfaceManager,
                loginUriProvider,
                platformComponents,
                pluginAccessor,
                pluginChecks,
                userManager,
                templateRenderer,
                textUtils,
                webResourceAssemblerFactory);
    }

    @Bean("textUtils")
    public TextUtils textUtils() {
        return new TextUtils();
    }
}
