package com.atlassian.devrel.rest;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.devrel.checks.PluginChecks;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

import static com.atlassian.devrel.rest.TraceCommentsResource.REST_PATH;
import static com.atlassian.devrel.rest.exception.RestApiExceptions.methodNotAllowed;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.WILDCARD;

@Path(REST_PATH) @jakarta.ws.rs.Path(REST_PATH)
@Consumes(WILDCARD) @jakarta.ws.rs.Consumes(WILDCARD)
@AnonymousAllowed @UnrestrictedAccess
@ParametersAreNonnullByDefault
public class TraceCommentsResource {
    static final String REST_PATH = "trace-comments";

    private static final List<String> SYSTEM_PROPERTY_TOGGLES = asList(
            "atl.html.trace.comments.soy-template-renderer.enabled",
            "atl.html.trace.comments.velocity-template-renderer.enabled",
            "atl.html.trace.comments.velocity.enabled",
            "atl.html.trace.comments.webpanels.enabled");

    private final PluginChecks pluginChecks;

    @Inject @jakarta.inject.Inject
    public TraceCommentsResource(PluginChecks pluginChecks) {
        this.pluginChecks = requireNonNull(pluginChecks, "pluginChecks");
    }

    @Produces(APPLICATION_JSON) @jakarta.ws.rs.Produces(APPLICATION_JSON)
    @GET@jakarta.ws.rs.GET
    @AnonymousAllowed @UnrestrictedAccess
    @XsrfProtectionExcluded
    public EnabledResponseBody enabled() {
        boolean atLeastOneSystemEnabled = SYSTEM_PROPERTY_TOGGLES.stream().anyMatch(Boolean::getBoolean);

        return new EnabledResponseBody(atLeastOneSystemEnabled);
    }

    @POST@jakarta.ws.rs.POST
    @AnonymousAllowed @UnrestrictedAccess
    @XsrfProtectionExcluded
    public void postEnable() {
        putEnable();
    }

    @PUT@jakarta.ws.rs.PUT
    @AnonymousAllowed @UnrestrictedAccess
    @XsrfProtectionExcluded
    public void putEnable() {
        if (!pluginChecks.isTraceCommentsAvailable()) {
            // not possible
            throw methodNotAllowed();
        }

        SYSTEM_PROPERTY_TOGGLES.forEach(property -> {
            System.setProperty(property, TRUE.toString());
        });
    }

    @DELETE@jakarta.ws.rs.DELETE
    @AnonymousAllowed @UnrestrictedAccess
    @XsrfProtectionExcluded
    public void disable() {
        SYSTEM_PROPERTY_TOGGLES.forEach(property -> {
            System.setProperty(property, FALSE.toString());
        });
    }

    private static class EnabledResponseBody {
        public boolean enabled;

        public EnabledResponseBody(boolean enabled) {
            this.enabled = enabled;
        }
    }
}
