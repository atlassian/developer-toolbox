package com.atlassian.devrel.rest.exception;

public class AlternateStatusCodeException extends RuntimeException {

    final int status;

    AlternateStatusCodeException(int status) {
        super();
        this.status = status;
    }
}
