package com.atlassian.devrel.rest.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.Provider;

/**
 * All because {@link Provider} doesn't work before Platform 8 (at least not on 6).
 * Throws the exception for you but "returns" the exception to keep the nicer syntax and avoid errors.
 */
public class RestApiExceptions {

    public static RuntimeException methodNotAllowed() throws WebApplicationException {
        return throwException(405);
    }

    private static RuntimeException throwException(int status) throws WebApplicationException {
        boolean isJavax;
        try {
            Class.forName("jakarta.ws.rs.Path");
            isJavax = false;
        } catch (ClassNotFoundException e) {
            isJavax = true;
        }

        if (isJavax) {
            throw new WebApplicationException(null, status);
        }
        throw new AlternateStatusCodeException(status);
    }
}
