package com.atlassian.devrel.checks;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.function.Function;

public class PluginChecks {
    @VisibleForTesting
    public static final String ATR_VELOCITY_PLUGIN_KEY = "com.atlassian.templaterenderer.atlassian-template-renderer-velocity1.6-plugin";
    private static final String DEVTOOLBOX_PLUGIN_KEY = "com.atlassian.devrel.developer-toolbox-plugin";
    private static final String PDE_PLUGIN_KEY = "com.atlassian.plugins.plugin-data-editor";
    private static final String QR_PLUGIN_KEY = "com.atlassian.labs.plugins.quickreload.reloader";
    @VisibleForTesting
    public static final String SOY_PLUGIN_KEY = "com.atlassian.soy.soy-template-plugin";

    private PluginAccessor pluginAccessor;

    public PluginChecks(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    public boolean isActiveObjectsAvailable() {
        return plugin("com.atlassian.activeobjects.activeobjects-plugin").isPresent();
    }

    public boolean isPluginDataEditorAvailable() {
        return plugin(PDE_PLUGIN_KEY).isPresent();
    }

    /**
     * This is also possible if web fragments core &ge; 7.1.0 or if velocity fork &ge; 1.6.4-atlassian-37, but CBF
     * getting library versions, they're all in Platform 7.1 anyway
     * @return whether the product supports enabling trace comments
     */
    public boolean isTraceCommentsAvailable() {
        // don't worry if they're disabled, if they are, we have bigger problems on our hands
        return  (pluginVersion(SOY_PLUGIN_KEY).compareTo("7.1.0") >= 0 ||
                // It's actually 6.2.0, but this allows us to cheat String#compareTo. Don't try this at home kids
                pluginVersion(ATR_VELOCITY_PLUGIN_KEY).compareTo("6.10.0") >= 0);
    }

    public boolean isQuickReloadAvailable() {
        return plugin(QR_PLUGIN_KEY).isPresent();
    }

    public String getQuickReloadVersion() {
        return pluginVersion(QR_PLUGIN_KEY);
    }

    public String getDevtoolboxVersion() {
        return pluginVersion(DEVTOOLBOX_PLUGIN_KEY);
    }

    private Optional<Plugin> plugin(@Nonnull final String pluginKey) {
        return Optional.ofNullable(pluginAccessor.getEnabledPlugin(pluginKey));
    }

    private String pluginVersion(@Nonnull final String pluginKey) {
        return plugin(pluginKey)
                .flatMap((Function<Plugin, Optional<String>>)
                        plugin -> Optional.of(plugin.getPluginInformation().getVersion()))
                .orElse("disabled");
    }
}
