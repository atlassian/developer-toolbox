// The QR mode should be:
// - "dev mode" when the toggle is "on"
// - "production mode" when the toggle is "off"
(function($, contextPath) {
    var batchStateUrl = contextPath + '/rest/qr/batching';
    var selector = '#dt-toolbar-qr-toggle';

    function avoidAuiBug(fn) {
        $(document).off('change', selector, handleQRtoggle);
        fn();
        $(document).on('change', selector, handleQRtoggle);
    }

    function handleQRtoggle(e) {
        var el = e.target;
        var shouldBeDevMode = el.checked === true;
        var prevState = !shouldBeDevMode;

        var result = $.ajax({
            url: batchStateUrl,
            method: shouldBeDevMode ? 'DELETE' : 'PUT',
            dataType: 'json'
        });

        $(selector).prop('busy', true);

        result.then(function(data) {
            console.log('QR mode changed successfully', data);
            avoidAuiBug(function() {
                $(selector)
                    .prop('busy', false)
                    .attr('checked', shouldBeDevMode);
            });
        }, function (err) {
            console.error('QR mode change failed', err);
            avoidAuiBug(function() {
                // reset to original state
                $(selector)
                    .prop('busy', false)
                    .attr('checked', prevState);
            });
        });

        e.preventDefault();
    }

    var checkState = $.ajax({
        url: batchStateUrl,
        method: 'GET',
        dataType: 'json'
    });

    $(selector)
        .prop('disabled', true)
        .prop('busy', true);

    // It's not easy finding out the state of this thing on the server-side, which is kinda sad.
    // ...so I need to do it all on the client-side.
    function reactToQRState() {
        checkState.then(function(data) {
            avoidAuiBug(function() {
                // Reflect the current state of QR in the UI component.
                var isDevMode = !data.batchingEnabled;
                let $selector = $(selector);
                $selector
                    .removeProp('disabled busy');
                if (isDevMode) {
                    $selector.attr('checked', true)
                } else {
                    $selector.removeAttr('checked')
                }
            });
        }, function(data) {
            console.error('could not determine current state of QR plugin and batching', data);
            $(selector).prop('disabled', true);
        });
    }

    // Initialize on page load...
    $(reactToQRState);

    // ...and make the toolbar capable of running this as well
    window.dtToolbar.initQR = reactToQRState;

}(AJS.$, AJS.contextPath()));
