(function($, contextPath) {
    var traceCommentStateUrl = contextPath + '/rest/dev-toolbox/1.0/trace-comments';
    var selector = '#dt-toolbar-trace-comments-toggle';

    function avoidAuiBug(fn) {
        $(document).off('change', selector, handleToggle);
        fn();
        $(document).on('change', selector, handleToggle);
    }

    function handleToggle(event) {
        var element = event.target;
        var futureState = Boolean(element.checked);
        var prevState = !futureState;

        var result = $.ajax({
            url: traceCommentStateUrl,
            method: futureState ? 'PUT' : 'DELETE',
            dataType: 'json'
        });

        $(selector).prop('busy', true);

        result.then(function(data) {
            console.count('HTML trace comments toggled successfully');
            avoidAuiBug(function() {
                $(selector)
                    .prop('busy', false)
                    .attr('checked', futureState);
            });
        }, function (err) {
            console.error('HTML trace comments toggle failed', err);
            avoidAuiBug(function() {
                // reset to original state
                $(selector)
                    .prop('busy', false)
                    .attr('checked', prevState);
            });
        });

        event.preventDefault();
    }

    var checkState = $.ajax({
        url: traceCommentStateUrl,
        method: 'GET',
        dataType: 'json'
    });

    $(selector)
        .prop('disabled', true)
        .prop('busy', true);

    // It's not easy finding out the state of this thing on the server-side, which is kinda sad.
    // ...so I need to do it all on the client-side.
    function reactToState() {
        checkState.then(function(data) {
            avoidAuiBug(function() {
                // Reflect the current state of QR in the UI component.
                let $toggle = $(selector);
                $toggle.removeProp('disabled busy');

                if (data.enabled) {
                    $toggle.attr('checked', true)
                } else {
                    $toggle.removeAttr('checked')
                }
            });
        }, function(data) {
            console.error('could not determine current state of QR plugin and batching', data);
            $(selector).prop('disabled', true);
        });
    }

    // Initialize on page load...
    $(reactToState);

    // ...and make the toolbar capable of running this as well
    window.dtToolbar.initTraceCommentState = reactToState;

}(AJS.$, AJS.contextPath()));
