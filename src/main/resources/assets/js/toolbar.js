AJS.$(function($){

    function getQueryParamByName(name){
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.search);
        if(results == null)
            return false;
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function getUri(){
        return window.location.protocol + '//' + window.location.host + window.location.pathname;
    }

    window.dtToolbar.init = function(toolbarHtml){
        const $body = $('body');
        var $toolbar = $body.append(toolbarHtml).find('#dt-toolbar');

        function toggleToolbar(){
            if($toolbar.hasClass('dt-expando-expanded')) {
                $toolbar.removeClass('dt-expando-expanded');
                localStorage.setItem('dev.toolbar.enabled',false);
            } else {
                $toolbar.addClass('dt-expando-expanded');
                localStorage.setItem('dev.toolbar.enabled',true);
            }
        }

        function toggleDTMenu(e){
            // DEVBOX-5 hack to fix the flyover menu being disabled when live reload is on
            if(!$(e.currentTarget).hasClass('dt-brand')) return;

            var $this = $('.dt-toolbox-btn');
            if($this.find('.dt-menu').hasClass('dt-expand')){
                $this.find('.dt-menu').removeClass('dt-expand');
                // $('html').trigger('dt:toolbox-menu:click');
            }else{
                $this.find('.dt-menu').addClass('dt-expand');
                // DEVBOX-5 hack to fix the flyover menu being disabled when live reload is on
                $('.dt-reload-on').attr('style','-webkit-animation-name: none; -moz-animation-name: none;');
            }
            e.stopPropagation();
        }

        function resetMenus(){
            $('.dt-menu.dt-expand').removeClass('dt-expand');

            // DEVBOX-5 hack to fix the flyover menu being disabled when live reload is on
            $('.dt-reload-on').attr('style','');
        }

        $body.on('click', resetMenus);
        $toolbar.on('click', '.dt-brand', toggleDTMenu);
        $toolbar.on('click', '.dt-expando',toggleToolbar);

        $toolbar.on('click', '#dt-highlight-i18n', function(){
            var i18nParam = getQueryParamByName('i18ntranslate');
            if(!i18nParam) {
                window.location = window.location.search ? getUri() + window.location.search + "&i18ntranslate=on" + window.location.hash : getUri() + "?i18ntranslate=on" + window.location.hash;
            } else {
                if(i18nParam === "on") {
                    window.location = window.location.href.replace("i18ntranslate=on","i18ntranslate=off");
                }else{
                    window.location = window.location.href.replace("i18ntranslate=off","i18ntranslate=on");
                }
            }
        });

        $toolbar.on('click', '#dt-bitbucket-web-fragments', function(){
            var paramsOn = /web\.panels&web\.items&web\.sections/g.test(window.location.search);
            if(!paramsOn) {
                window.location = window.location.search ? getUri() + window.location.search + "&web.panels&web.items&web.sections" + window.location.hash : getUri() + "?web.panels&web.items&web.sections" + window.location.hash;
            } else {
                if(/\?web\.panels&web\.items&web\.sections&/.test(window.location.search)) {
                    window.location = window.location.href.replace(/\?web\.panels&web\.items&web\.sections&/,"?");
                } else {
                    window.location = window.location.href.replace(/[&\?]web\.panels&web\.items&web\.sections/,"");
                }
            }
        });

        window.dtToolbar.initQR && window.dtToolbar.initQR();
        window.dtToolbar.initTraceCommentState && window.dtToolbar.initTraceCommentState();

        if(localStorage.getItem('dev.toolbar.enabled') === "true") {
            toggleToolbar();
        }
    };

    $.ajax({
        type: 'GET',
        url: AJS.contextPath() + "/plugins/servlet/dev-toolbar",
        dataType: 'html',
        success: dtToolbar.init
    });
});
