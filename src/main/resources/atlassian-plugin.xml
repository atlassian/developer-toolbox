<?xml version="1.0" encoding="UTF-8"?>

<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}"/>
    </plugin-info>
    <resource type="i18n" name="i18n" location="developer-toolbox"/>

    <rest-migration key="rest-migration-key">
        <rest-v2/>
    </rest-migration>

    <rest key="rest" path="/dev-toolbox"  version="1.0">
        <package>com.atlassian.devrel.rest</package>
    </rest>

    <servlet name="Homepage Servlet (javax)" key="homepage-servlet" class="com.atlassian.devrel.servlet.JavaxServlet">
        <description>Serves the toolbox home page HTML.</description>

        <restrict application="bamboo" version="[0,11)"/>
        <restrict application="bitbucket" version="[0,10)"/>
        <restrict application="confluence" version="[0,10)"/>
        <restrict application="crowd" version="[0,7)"/>
        <restrict application="jira" version="[0,11)"/>
        <restrict application="refapp" version="[0,8)"/>

        <url-pattern>/developer-toolbox</url-pattern>

        <url-pattern>/dev-toolbar</url-pattern>

        <!-- Sandbox -->
        <url-pattern>/dev-sandbox</url-pattern>
        <url-pattern>/dev-sandbox/*</url-pattern>
        <url-pattern>/empty</url-pattern>
        <url-pattern>/empty/*</url-pattern>
    </servlet>

    <servlet name="Homepage Servlet (jakarta)" key="homepage-servlet" class="com.atlassian.devrel.servlet.JakartaServlet">
        <description>Serves the toolbox home page HTML.</description>

        <restrict application="bamboo" version="[11,)"/>
        <restrict application="bitbucket" version="[10,)"/>
        <restrict application="confluence" version="[10,)"/>
        <restrict application="crowd" version="[7,)"/>
        <restrict application="jira" version="[11,)"/>
        <restrict application="refapp" version="[8,)"/>

        <url-pattern>/developer-toolbox</url-pattern>

        <url-pattern>/dev-toolbar</url-pattern>

        <!-- Sandbox -->
        <url-pattern>/dev-sandbox</url-pattern>
        <url-pattern>/dev-sandbox/*</url-pattern>
        <url-pattern>/empty</url-pattern>
        <url-pattern>/empty/*</url-pattern>
    </servlet>

    <!--
        Web resources for the toolbox homepage
     -->
    <web-resource key="homepage-resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.auiplugin:aui-button</dependency>
        <dependency>com.atlassian.auiplugin:aui-toolbar2</dependency>
        <dependency>com.atlassian.plugins.atlassian-plugins-webresource-plugin:context-path</dependency>
        <dependency>:toggle-buttons</dependency>
        <resource name="images/" type="download" location="/assets/homepage/images/"/>
        <resource name="homepage.css" type="download" location="/assets/homepage/css/homepage.css"/>
        <resource name="homepage.js" type="download" location="/assets/homepage/js/homepage.js"/>
    </web-resource>

    <!--
        Web items for displaying the Dev Toolbox link in the product administration consoles.
     -->
    <web-item name="Developer Toolbox" i18n-name-key="developer-toolbox.name-jira" key="developer-toolbox-link-jira"
              section="top_system_section/advanced_section" weight="1000"
              application="jira">
        <description key="developer-toolbox.description">Developer Toolbox</description>
        <label key="developer-toolbox.label"/>
        <link linkId="developer-toolbox-link">/plugins/servlet/developer-toolbox</link>
    </web-item>

    <web-item name="Developer Toolbox" i18n-name-key="developer-toolbox.name.jira" key="user-profile-developer-toolbox-link-jira"
              section="system.user.options/jira-help"
              application="jira">
        <description key="developer-toolbox.description">Developer Toolbox</description>
        <label key="developer-toolbox.label"/>
        <link linkId="developer-toolbox-link">/plugins/servlet/developer-toolbox</link>
    </web-item>
    <!-- END web items for displaying REST Browser link in admin consoles -->

    <web-resource key="dt-toolbar-namespace">
        <resource name="dt-toolbar-namespace.js" type="download" location="assets/js/namespace.js"/>
    </web-resource>

    <web-resource key="dt-toolbar">
        <dependency>:dt-toolbar-base</dependency>
        <context>atl.general</context>
        <context>atl.admin</context>
        <context>atl.userprofile</context>
        <context>atl.popup</context>
        <context>atl.livereload</context>
        <context>dev.toolbar</context>
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>:dt-toolbar-namespace</dependency>
        <dependency>:toggle-buttons</dependency>
        <resource name="toolbar.js" type="download" location="assets/js/toolbar.js"/>
        <resource name="toolbar.css" type="download" location="/assets/css/toolbar.css"/>
        <resource name="ajax-loader.gif" type="download" location="/assets/homepage/images/ajax-loader.gif"/>
    </web-resource>

    <web-resource key="toggle-buttons">
        <dependency>:dt-toolbar-namespace</dependency>
        <dependency>com.atlassian.plugins.atlassian-plugins-webresource-plugin:context-path</dependency>
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.auiplugin:aui-toggle</dependency>
        <dependency>com.atlassian.auiplugin:toggle</dependency>
        <dependency>com.atlassian.auiplugin:aui-tooltips</dependency>
        <dependency>com.atlassian.auiplugin:aui-tooltip</dependency>
        <dependency>com.atlassian.auiplugin:tooltip</dependency>
        <resource name="qr-toggle.css" type="download" location="features/quickreload/quickreload-toggle.css"/>
        <resource name="qr-toggle.js" type="download" location="features/quickreload/quickreload-toggle.js"/>
        <resource name="trace-comments-toggle.css" type="download" location="features/trace-comments/trace-comments-toggle.css"/>
        <resource name="trace-comments-toggle.js" type="download" location="features/trace-comments/trace-comments-toggle.js"/>
    </web-resource>

    <resource name="images/" type="download" location="assets/images/"/>

    <web-item name="Sandbox Servlet Link" key="sandbox-servlet-toolbar-item"
              section="dev-toolbar-menu" weight="1">
        <tooltip key="sandbox-servlet.description">Sandbox Servlet</tooltip>
        <label key="sandbox-servlet.label"/>
        <link linkId="dt-sandbox-servlet">/plugins/servlet/empty</link>
    </web-item>

    <web-item name="Highlight i18n" key="highlight-i18n-toolbar-item"
              section="dev-toolbar" weight="10">
        <tooltip key="highlight-i18n.description">Highlight i18n</tooltip>
        <label key="highlight-i18n.label"/>
        <link linkId="dt-highlight-i18n"/>
        <param name="type" value="image"/>
        <condition class="com.atlassian.devrel.condition.HighlightI18NEnabledCondition"/>
    </web-item>

    <web-item name="Show Stash Web Fragments" key="stash-web-fragments-toolbar-item"
              section="dev-toolbar" weight="10" application="stash">
        <tooltip key="show-stash-web-fragements.description">Highlight Stash Web Fragments</tooltip>
        <label key="show-stash-web-fragments.label"/>
        <link linkId="dt-bitbucket-web-fragments"/>
        <param name="type" value="image"/>
    </web-item>

    <web-item name="Show Bitbucket Web Fragments" key="bitbucket-web-fragments-toolbar-item"
              section="dev-toolbar" weight="10" application="bitbucket">
        <tooltip key="show-bitbucket-web-fragments.description">Highlight Bitbucket Web Fragments</tooltip>
        <label key="show-bitbucket-web-fragments.label"/>
        <link linkId="dt-bitbucket-web-fragments"/>
        <param name="type" value="image"/>
    </web-item>

</atlassian-plugin>
