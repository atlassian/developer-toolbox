Atlassian Developer Toolbox Plugin
==================================

The Atlassian Developer Toolbox is a set of tools used to improve developer productivity. It includes the REST API Browser which allows a developer to browse, discover, and test Atlassian's rich REST and JSON-RPC APIs.

After you install the Developer Toolbox, go into the product's Administration page. You'll find the "Developer Toolbox" category on that page.

For more information, visit the [Marketplace listing](https://marketplace.atlassian.com/plugins/com.atlassian.devrel.developer-toolbox-plugin).
