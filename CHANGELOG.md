# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.2.0] - 2025-02-20

### Changed

- [DCPL-2272](https://ecosystem.atlassian.net/browse/DCPL-2272): Remove REST API Browser (useless and broken in Plat 7+)

## [3.1.2] - 2025-02-20

### Fixed

- [DCPL-2272](https://ecosystem.atlassian.net/browse/DCPL-2272): Fix empty servlet in Confluence 9.1
- [DCPL-2272](https://ecosystem.atlassian.net/browse/DCPL-2272): Jakarta support
- [DCPL-2272](https://ecosystem.atlassian.net/browse/DCPL-2272): Fix REST API links & toolbar web-item

## [3.1.1] - 2024-09-03

### Fixed

- [DEVBOX-59](https://ecosystem.atlassian.net/browse/DEVBOX-59): Failing to start in Jira (all Jira versions)

## [3.1.0] - 2024-08-15
### Added
- [DCPL-1177](https://ecosystem.atlassian.net/browse/DCPL-1177): Add toggle for HTML traceability

### Changed
- Removed the dev mode condition from the toolbar which makes it hard to enable dev mode (bootstrap problem)
- Make toolbar menu items larger so they're easier to click

### Fixed
- Dev Mode toggle not starting in the right position
- Toolbar menu links not being full width
- Increased width of toolbar menu so links aren't truncated

## [3.0.5] - 2024-03-15
### Fixed
- [DCPL-1104](https://ecosystem.atlassian.net/browse/DCPL-1104): Make forward compatible with Plat 7 milestone

## [3.0.4] - 2023-08-04
- [DEVBOX-57](https://ecosystem.atlassian.net/browse/DEVBOX-57): Make transformer-less for faster startup
- chore: Remove usage of Guava

## [3.0.3] - 2023-08-02
### Fixed

- [RAB-38](https://ecosystem.atlassian.net/browse/RAB-38): Fix REST API browser in FeCru
- [RAB-39](https://ecosystem.atlassian.net/browse/RAB-39): Fix missing spinner
- [RAB-41](https://ecosystem.atlassian.net/browse/RAB-41): Add admin web-item for REST API browser in Crowd
- [RAB-42](https://ecosystem.atlassian.net/browse/RAB-42): Make it obvious when the list of APIs is empty

## [3.0.2] - 2023-02-13
### Fixed

- [BSP-4684](https://bulldog.internal.atlassian.com/browse/BSP-4684) Update dependency and product versions
- [BSP-4685](https://bulldog.internal.atlassian.com/browse/BSP-4685) Remove "Atlassian Plugin Checkup" service link
- [BSP-4686](https://bulldog.internal.atlassian.com/browse/BSP-4686) AUI docs link broken
- [BSP-4689](https://bulldog.internal.atlassian.com/browse/BSP-4689) "Highlight i18n" button is very hard to read 

## [3.0.1] - 2020-10-13
## [3.0.0] - 2020-10-12

[unreleased]: https://bitbucket.org/atlassian/developer-toolbox/branches/compare/master%0Ddeveloper-toolbox-plugin-3.1.1
[3.1.1]: https://bitbucket.org/atlassian/developer-toolbox/branches/compare/developer-toolbox-plugin-3.1.1%0Ddeveloper-toolbox-plugin-3.1.0
[3.1.0]: https://bitbucket.org/atlassian/developer-toolbox/branches/compare/developer-toolbox-plugin-3.1.0%0Ddeveloper-toolbox-plugin-3.0.8
[3.0.5]: https://bitbucket.org/atlassian/developer-toolbox/branches/compare/developer-toolbox-plugin-3.0.5%0Ddeveloper-toolbox-plugin-3.0.4
[3.0.4]: https://bitbucket.org/atlassian/developer-toolbox/branches/compare/developer-toolbox-plugin-3.0.4%0Ddeveloper-toolbox-plugin-3.0.3
[3.0.3]: https://bitbucket.org/atlassian/developer-toolbox/branches/compare/developer-toolbox-plugin-3.0.3%0Ddeveloper-toolbox-plugin-3.0.2
[3.0.2]: https://bitbucket.org/atlassian/developer-toolbox/branches/compare/developer-toolbox-plugin-3.0.2%0Ddeveloper-toolbox-plugin-3.0.1
[3.0.1]: https://bitbucket.org/atlassian/developer-toolbox/branches/compare/developer-toolbox-plugin-3.0.1%0Ddeveloper-toolbox-plugin-3.0.0
